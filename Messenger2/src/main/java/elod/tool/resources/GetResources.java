package elod.tool.resources;
//Sigar Lib
//"/home/ilias/Downloads/lib"

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.SigarException;

import elod.tool.msg.Notifications;
import elod.tool.msg.Resources;

public class GetResources {

	/**
	 * @param args the arguments for the application to start
	 * args[0] the path to the Sigar lib files
	 * args[1] the file name with full path of the output file.
	 * args[2] the machines IP
	 * args[3] the machine title/description
	 */
	public static void main(String[] args) {
//		laptop
		//args=new String[]{"src/main/resources/Sigar_lib/","/home/ilias/temp/resources.txt","192.1.1.1","test"};
	
		//Desktop
	//	args=new String[]{"/media/ilias/4f3fe2dc-233f-4e74-a059-cce940316dbd/home/ilias/Downloads/lib","/media/ilias/4f3fe2dc-233f-4e74-a059-cce940316dbd/home/ilias/Downloads/00.txt", "9.9.9.9", "text test"};
		PrintWriter writer = null;
		//		check args
		
		if(args.length<4){
			System.out.println("Wrong parameters.\nUsage:\n\tthe path to the Sigar lib files\n\tthe file name with full path of the output file \n\tthe machines IP \n\tthe machine title/description");
			System.exit(-1);
		}
		try { 
			writer = new PrintWriter(new FileWriter(args[1], true)); 
			
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			System.exit(-1);
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//get resources stats and save to file
		
	
		// TODO Auto-generated method stub
		Notifications notif =new Notifications();
		System.setProperty("org.hyperic.sigar.path",args[0]);

		Resources rs=new Resources(new Sigar(),args[0]);
		
		try {
			for (int i=0;i<6;i++){
			writer.append("IP\t"+args[2]+"\tdesc\t"+args[3]+"\tCPU\t"+rs.getAllCpus()+"\tMem\t"+rs.getMemory()+"\tNet\t"+rs.getNetwork()+"\tHdd\t"+rs.getFreeHddPerc()+"\ttime\t"+new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()).toString()+"\n");
			writer.flush();
			Thread.sleep(5300);
			}
			writer.close();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SigarException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
