package elod.tool.msg;
/*
/home/ilias/skaros/Dropbox/msgs.csv -u root -p salonika -a jdbc:mysql://127.0.0.1:3306/messenger -as sourceAS1 -at titleAT1 -dn devDN1 -au userAU1
*/

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import elod.tool.msg.MachineStats;
import elod.tool.msg.Machines;
import elod.tool.msg.Resources;
import elod.tool.msg.Stats;
import elod.tool.msg.msgBody;
//import elod.tool.msg.Messenger2.Notifications;

import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.SigarException;

/**
 * the messenger class
 * @author ilias
 *
 */
public class Notifications 
{
	/**
	 * the name of the software programmer
	 */
	public String devName=null;
	/**
	 * the name of the software
	 */
	public String appName=null;
	/**
	 * the name of the user of the software
	 */
	public String userName=null;
	/**
	 * The title of the source
	 */
	public String source=null;
	/**
	 * the id of the source
	 */
	public String SourceID=null;
	/**
	 * the id of the sofware
	 */
	public String SoftwareID=null;
	
	/**
	 * the database username
	 */
	public String DB_USER=null;
	/**
	 * the database password
	 */
	public String DB_PASS=null;
	/**
	 * the database full address
	 */
	public String DB_ADDRS=null;
	/**
	 * a flag to notify that the basic info (names, titles etch) have been stored already
	 */
	private boolean inserted=false;

	private String Email_User=null;
	private String Email_Pass=null;
	private String[] Email_Recipients=null;
	private Date start;
//	boolean sources=false;
	

	/**
	 * the execution id for the session
	 */
	int execId=-1;
	
	/**
	 * an Array list that holds the messages to be passed
	 */
	List <msgBody> messageList=new ArrayList<msgBody>();
	
	/**
	 * contains information of a machine and the stats of it.
	 * the information should be passed in a List of Strings
	 */
	
	List<MachineStats> MachineStats=new ArrayList<MachineStats>();
	
	/**
	 * An Array list containing objects of machines
	 */
	List<Machines> machines=new ArrayList<Machines>();
	
	/**
	 * Constructor
	 * set up the necessary data prior of inserting to the database
	 * @param sourceId the source of the data
	 * @param user the user
	 * @param app The application that created the message.
	 * @param dev The name of the developer
	 */
		public Notifications(String sourceId,String softwareId,String user,String app,String dev){
			SourceID=sourceId; 
			SoftwareID=softwareId;
		userName=user;
		devName=dev;
		appName=app;
		start=new Date();
	}
		
	/**
	 * constructor. Creates an object using a file with the nesessery data,
	 * Developer,Application,User and Source
	 * @param settings
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	
	
		public Notifications(String settings) throws FileNotFoundException, IOException{
		Properties prop = new Properties();
	
			prop.load(new FileInputStream(settings));
		
			devName= prop.getProperty("Developer");
			appName=prop.getProperty("Application");
			userName=prop.getProperty("User");
			SourceID=prop.getProperty("Source");	
			SoftwareID=prop.getProperty("Software");
			start=new Date();
		
	//execute.DbCredentials(new File(args[i+1]));
	}
	
		/**
		 * Empty constructor. 
		 * This must be used with the addition of each element individually
		 * @see #setDev(String dev)
		 * @see #setApp(String app) 
		 *  @see #setUser(String user)
		 *  @see #setSource(String source)
		 */
		public Notifications(){
			start=new Date();
		}
		
		/**
		 * Sets the id for the source this software is handling. This is manually retrieved
		 * @param Source The id of the source
		 * @param Software The id of the software
		 */
		public void setSourcesIds(String Source,String Software){
			SourceID=Source;
			SoftwareID=Software;
		}
	
	/**
	 * sets the developers name that will be used for the messages
	 * This is the name of the developer of the application that is used
	 * @param dev The name of the developer
	 */
	public void setDev(String dev){
		devName=dev;
	}
	
	/**
	 * sets the name of the application that created the message. 
	 * This should be identical to the other instances, in order to update the database table properly
	 * @param app The application that created the message.
	 */
	public void setApp(String app){
		this.appName=app;
	}
	
	/** 
	 * sets the name of the user that executed the application
	 * This could be empty or null
	 * @param user the user
	 */
	public void setUser(String user){
		this.userName=user;
	}
	
	/**
	 * sets the source that the application is handling. For example it can be "Espa", "Diaygeia" etch.
	 * This should be identical to the other instances, in order to update the database table properly
	 * @param source the source of the data
	 */
	public void setSource(String source){

		this.source=source;
	
	}
	public void setSourceId(String id){
		this.SourceID=id;
	}
	
	public String getSource(){
		return this.source;
	}
	public String getSourceId(){
		return this.SourceID;
	}
	

	
	public static void main(String[] args) {	
		/*
/home/ilias/Documents/pireus/messengerTest/testfile.txt /home/ilias/Documents/pireus/messengerTest/Stats.txt -f /home/ilias/Documents/pireus/messengerTest/TextMessengerSettings.txt 
	 */
	System.setProperty("org.hyperic.sigar.path","~/.m2/repository/org/gridkit/lab/sigar-lib/1.6.4");///home/ilias/Downloads/lib");
		
		if(args.length<2){
			//wrong parameters
			System.out.println("Usage Messenger.jar [pathToMessageFile] [pathToSystemStatsFile] [application information]  [DB Authentication] ");
			System.out.println("\t [pathToMessageFile]: the full path to the file containing the messages to be uploaded");
			System.out.println("\t [pathToSystemStatsFile]: the fill path to the file containing the statistics of the system. enter \"0\" if not applicable");
			System.out.println("\t [application information] please enter the following");
			System.out.println("\t\t-as [The application Source]\n\t\t-at [The application Title]\n\t\t-dn [The developers name]\n\t\t-au [The application user]");
			System.out.println("\t [DB Authentication]: the authentication parameters for the Database");
			System.out.println("\t This might be:\n\t\t-f: a file containing the authentication");
			System.out.println("\t OR\n\t\t-u: the username\n\t\t-p: the password\n\t\t-a: the DB address");
			System.exit(-1);
		}
		Notifications execute= new Notifications();
				for(int i=2;i<args.length;i=i+2){
						switch(args[i]){
					case "-f":
					case "-F":{		
						try {
							execute.DbCredentials(new File(args[i+1]));
						} catch (FileNotFoundException e){
							System.out.println("FileNotFoundException on "+args[i+1]+" \nMessage:"+e.getMessage()+"\n"+e.getLocalizedMessage());
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							System.out.println("IOException on "+args[i+1]+" \nMessage:"+e.getMessage()+"\n"+e.getLocalizedMessage());
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
//						There is a file containning DB authentication
						break;
					}
					case "-u":
					case "-U":{
						execute.setDbUser(args[i+1]);
							break;
						}
					case "-p":
					case "-P":{
						execute.setDbPass(args[i+1]);
							break;
						}
					case "-a":
					case "-A":{
						execute.setDbAdrs(args[i+1]);
						break;
						}						
					}//switch
					if(args[i].equalsIgnoreCase("-as")){
						//the source of data for the application						
						execute.setSource(args[i+1]);
					}else if(args[i].equalsIgnoreCase("-at")){
						//the application name
						execute.setApp(args[i+1]);
//						appName=args[i+1];
					}else if(args[i].equalsIgnoreCase("-dn")){
						//the developers name
						execute.setDev(args[i+1]);
					}else if(args[i].equalsIgnoreCase("-au")){
						//the user of the application
						execute.setUser(args[i+1]);
					}					
				}//for
				try {
					execute.readMsgFile(args[0]);
				} catch (IOException e1) {
					System.out.println("IOException on reading the messages file\nMessage:"+e1.getMessage()+"\n"+e1.getLocalizedMessage());
					e1.printStackTrace();
				}	
				if (!args[1].equals("0")&&!args[1].equals("\"0\"")){
					// a file containing machine system data is present
					try {
						execute.readStatFile(args[1]);
					} catch (IOException e) {
						System.out.println("IOException while reading the stats file. does it exist?\nMessage:"+e.getMessage()+"\n"+e.getLocalizedMessage());
						e.printStackTrace();
					}
				}
		
				try {
					execute.runInsert();
				} catch (SQLException e) {
					System.out.println("SQL Exception while processing data\nMessage:"+e.getMessage()+"\n"+e.getLocalizedMessage()+"\ncode:"+e.getErrorCode());
					e.printStackTrace();
				}
		
				try {
					execute.finishExecution();
				} catch (SQLException e) {
					System.out.println("SQLException while executing finishExecution\nMessage:"+e.getMessage()+"\n"+e.getLocalizedMessage()+"\nCode:"+e.getErrorCode());
					e.printStackTrace();
				}
	}//main
	
	/**
	 * take the system resources values and creates a new MachineStats object that is added to the list
	 * @param ip
	 * @param computerTitle
	 * @throws SigarException 
	 * @throws InterruptedException 
	 * @throws Exception 
	 */
	public void addMachineStat(String ip,String computerTitle) throws InterruptedException, SigarException {
		Resources rs=new Resources(new Sigar());
		addMachineStat(ip,computerTitle,Double.toString(rs.getAllCpus()),Double.toString(rs.getMemory()),rs.getNetwork(),Double.toString(rs.getFreeHddPerc()));
	
	}
	
	public void loadLibs(String path){
		System.setProperty("org.hyperic.sigar.path",path);///home/ilias/Downloads/lib");
	}
	
	public void addMachineStat(String ip,String computerTitle,String cpu,String ram, String ntw,String drive){
		List <Machines> machines=new ArrayList<Machines>();
		List<Stats> stats =new ArrayList<Stats>();
		Machines machine=new Machines(ip,computerTitle);
		Stats stat=new Stats(new String[]{cpu,ram,ntw,drive});
		machines.add(machine);
		stats.add(stat);

		MachineStats.add(new MachineStats(machine,stat));
	}
	
	/**
	 * load the machine info and stats from a file using names for each item.
	 *  The file should be in the format
	 * ip=[the IP]
	 * title=[some free text]
	 * Can also (not necessarily) add the recourses stats of this machine.
	 * cpu=[cpu%]
	 * ram=[free ram]
	 * ntw=[up/down speed of network]
	 * drive=[free% of disk]
	 * @param fileName
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws SigarException 
	 * @throws InterruptedException 
	 */
	public void addMachineStat(String fileName) throws FileNotFoundException, IOException, InterruptedException, SigarException{
		Properties prop = new Properties();
		
			prop.load(new FileInputStream(fileName));
			
			String ip = null,title = null,cpu = null,ram = null,ntw = null,drive = null;
			//get the machines information
			if(prop.getProperty("ip")!=null)
				 ip= prop.getProperty("ip");			
			if(prop.getProperty("title")!=null)
			 title= prop.getProperty("title");
			
			//get the stats
			if(prop.getProperty("cpu")!=null)
				cpu= prop.getProperty("cpu");
			if(prop.getProperty("ram")!=null)
				ram= prop.getProperty("ram");
			if(prop.getProperty("ntw")!=null)
				ntw= prop.getProperty("ntw");
			if(prop.getProperty("drive")!=null){
				drive= prop.getProperty("drive");
				addMachineStat(ip,title,cpu,ram,ntw,drive);
				}else{
					addMachineStat(ip,title);
			}
	}

	/**
	 * when the execution of an application is finished, this method will set the finish record on the database to the current date/time
	 * @throws SQLException 
	 */
	public void finishExecution() throws SQLException{
		Connection conn;
	
		conn = DriverManager.getConnection(DB_ADDRS,DB_USER,DB_PASS);
		Statement st = conn.createStatement();
		
		st.executeUpdate("update Executions set Finish='"+new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()).toString() +"' where Id='"+execId+"';");
		st.close();
	}
	
	
	
	public void readStatFile(String filepath) throws IOException{
		File file=new File(filepath);
		String ip = null,computerTitle= null,cpu= null,ram= null,ntw= null,drive= null;
		if(file.isFile()){
			String data;
			BufferedReader br = null;	
			
				br = new BufferedReader(new FileReader(file));
				while ((data = br.readLine()) != null) {
					String[] dataArray=data.split("\t");
					for( int c=0;c<dataArray.length;c=c+2)
					{switch (dataArray[c]){
					
						case "IP": {
							//The ip of the machine
							ip=dataArray[c+1];
						}
						case "desc":{
							//The machine description
							computerTitle=dataArray[c+1];
						}
						case "CPU":{
							//The CPU information
							cpu=dataArray[c+1];
						}
						case "Mem":{
							//The Ram information
							ram=dataArray[c+1];
						}
						case "Net":{
							//Network trafic
							ntw=dataArray[c+1];
						}
						case "Hdd":{
							//Hard disk space
							drive=dataArray[c+1];							
						}
						
					}//switch
				
					}//for
					addMachineStat(ip,computerTitle,cpu,ram,ntw,drive);
				}//while
				br.close();			
		}else System.out.println("not a file"); 		
	}
	
	
	public  void readMsgFile(String filepath) throws IOException{
		File file=new File(filepath);
		if(file.isFile()){
			String data;
			BufferedReader br = null;
	
				br = new BufferedReader(new InputStreamReader(
					    new FileInputStream(file), "UTF-8"));
//	new BufferedReader(new FileReader(file));
				while ((data = br.readLine()) != null) {
					if (data.length()<5)continue ;
					String[] dataArray=data.split("\t");
						boolean publicPrivate=Boolean.parseBoolean(dataArray[3]);
						
					messageList.add(new msgBody(dataArray[0],dataArray[1],dataArray[2],publicPrivate,dataArray[4]));
					if(dataArray[2].equals("5")&&Email_User!=null&&Email_Pass!=null&&Email_Recipients!=null){
						sendFromGMail( Email_User,Email_Pass,Email_Recipients,"Critical Error warning!",dataArray[1]+"\n"+dataArray[4]);
					
					}
				}
				br.close();
		}else System.out.println("not a file");
	}
	
	
	
	/**
	 * set up the necessary data prior of inserting to the database
	 * @param sourceId the source ID of the data
	 * @param user the user
	 * @param app The application that created the message.
	 * @param dev The name of the developer
	 */
	public void initialise(String sourceId,String softwareId,String user,String app,String dev){
		SourceID=sourceId; 
		SoftwareID=softwareId;
		userName=user;
		devName=dev;
		appName=app;
	}
	
	/**
	 * set the database authentication parameters
	 * @param user the database username
	 * @param pass the password
	 * @param adrs the database url
	 */
	public void DbCredentials(String user,String pass,String adrs){
		 DB_USER=user;
		 DB_PASS=pass;
		 DB_ADDRS=adrs;
		 //TODO connect to DB
	}
	/**
	 * set the gmail account settings and the list of recipients
	 * @param user The google id, with out the "@gmail.com"
	 * @param password The password of the account
	 * @param recipients a String array containing the recipients
	 */
	public void setEmail(String user,String password,String[] recipients){
		if(user!=null&&password!=null&&recipients!=null){
			Email_User=user;
			Email_Pass=password;
			Email_Recipients=recipients;
		}
	}
	/**
	 * returns the gmail account name that is set to be used
	 * @return String, the gmail user name
	 */
	public String getEmailUser(){
		return Email_User;
	}
	/**
	 * returns the password set for use with the gmail account
	 * @return String the password
	 */
	public String getEmailPassword(){
		return Email_Pass;
	}
	/**
	 * 
	 * @return
	 */
	public String[] getEmailRecipients(){
		return Email_Recipients;
	}
	/**
	 * get the file containing the authentication parameters for the db connection
	 * @param File the properties file containing the DB authentication
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public void DbCredentials(File credentials) throws FileNotFoundException, IOException{
		Properties prop = new Properties();
		prop.load(new FileInputStream(credentials));
		DB_PASS= prop.getProperty("Password");
		DB_USER= prop.getProperty("Username");
		DB_ADDRS= prop.getProperty("DataBase");
		  		System.out.println(DB_PASS+DB_USER+DB_ADDRS);

		Email_User=prop.getProperty("Email_User");
		Email_Pass=prop.getProperty("Email_Password");
		String test=prop.getProperty("Recipients");
		if(test!=null)
				Email_Recipients=test.split(",");

		
		if(prop.getProperty("Developer")!=null)
			devName= prop.getProperty("Developer");
		if(prop.getProperty("Application")!=null)
			appName=prop.getProperty("Application");
		if(prop.getProperty("User")!=null)
			userName=prop.getProperty("User");
		if(prop.getProperty("Source")!=null)
			SourceID=prop.getProperty("Source");
		if(prop.getProperty("Software")!=null)
			SoftwareID=prop.getProperty("Software");			
	
		 //TODO connect to DB
	}
	
	/**
	 * set the database user name
	 * @param user the database username
	 */
	public void setDbUser(String user){
		 DB_USER=user;
	}
	
	/**
	 * set the database password
	 * @param pass the database password
	 */
	public void setDbPass(String pass){
		 DB_PASS=pass;
	}
	
	/**
	 * set the database address
	 * @param adrs the database address
	 */
	public void setDbAdrs(String adrs){
		 DB_ADDRS=adrs;
	}
	/**
	 * receives the message to be saved to the database including the free text value 
	 * and calls the add() method from which the insertion is done
	 * @param msg the message to be saved
	 * @param MessageType the type of the message (ie success error etc)
	 * @param publicPrivate true if this message is public, false if it is private
	 * @param freeText some explanation of the message
	 */
	public void addMessage(String msg,String MessageType,boolean publicPrivate,String freeText) {
		add(msg,MessageType,publicPrivate,freeText);
//		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
//		messageList.add(new msgBody(dateFormat.format(new Date()),msg,MessageType,publicPrivate,freeText));
//		if(MessageType.equals("5")&&Email_User!=null&&Email_Pass!=null&&Email_Recipients!=null){
//			sendFromGMail( Email_User,Email_Pass,Email_Recipients,"Critical Error warning!",msg+"\n"+freeText);
//		
//		}
	}
	/**
	 * receives the message to be saved to the database with out the free text value 
	 * and calls the add() method, using null as the free text, from which the insertion is done
	 * @param msg the message to be saved
	 * @param MessageType the type of the message (ie success error etc)
	 * @param publicPrivate true if this message is public, false if it is private
	 */
	public void addMessage(String msg,String MessageType,boolean publicPrivate) {
		add(msg,MessageType,publicPrivate,null);
//		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
//		messageList.add(new msgBody(dateFormat.format(new Date()),msg,MessageType,publicPrivate,null));
//		if(MessageType.equals("5")&&Email_User!=null&&Email_Pass!=null&&Email_Recipients!=null){
//			sendFromGMail( Email_User,Email_Pass,Email_Recipients,"Critical Error warning!",msg);
//		
//		}
	}
	/**
	 * adds the message received to the message ArrayList;
	 * @param msg the message to be saved
	 * @param MessageType the type of the message (ie success error etc)
	 * @param publicPrivate true if this message is public, false if it is private
	 * @param freeText some (if any) explanation of the message
	 */
	private void add(String msg,String MessageType,boolean publicPrivate,String freeText){
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		messageList.add(new msgBody(dateFormat.format(new Date()),msg,MessageType,publicPrivate,freeText));
		if(MessageType.equals("5")&&Email_User!=null&&Email_Pass!=null&&Email_Recipients!=null){
			sendFromGMail( Email_User,Email_Pass,Email_Recipients,"Critical Error warning! from "+appName,msg+"\n"+freeText);
		
		}
	}
	
	
	/**
	 * send ALL the messages passed to the specified recipients, using a gmail account
	 * @param user the username of the gmail account, without the "@gmail.com"
	 * @param password the password of the account
	 * @param recepients a String array containing the recipients.
	 * @param subject the subject of the email
	 */
	
	private static void sendFromGMail(String from, String pass, String[] to, String subject, String body) {
		if(from==null||pass==null||to==null){
			return;
		}else{
	        Properties props = System.getProperties();
	        String host = "smtp.gmail.com";
	        props.put("mail.smtp.starttls.enable", "true");
	        props.put("mail.smtp.host", host);
	        props.put("mail.smtp.user", from);
	        props.put("mail.smtp.password", pass);
	        props.put("mail.smtp.port", "587");
	        props.put("mail.smtp.auth", "true");
	        Session session =Session.getInstance(props); //Session.getDefaultInstance(props);
	        MimeMessage message = new MimeMessage(session);
	
	        try {
	            message.setFrom(new InternetAddress(from));
	            InternetAddress[] toAddress = new InternetAddress[to.length];
	
	            // To get the array of addresses
	            for( int i = 0; i < to.length; i++ ) {
	            	
	                message.addRecipient(Message.RecipientType.TO, new InternetAddress(to[i]));
	            }
	            message.setSubject(subject);
	            message.setText(body);
	            Transport transport = session.getTransport("smtp");
	            transport.connect(host, from, pass);
	            transport.sendMessage(message, message.getAllRecipients());
	            transport.close();
	        }
	        catch (AddressException ae) {
	            System.out.println("error"+ae.getLocalizedMessage());
	//            notif.addWarning("can not sent email. AddressException " +ae.getLocalizedMessage());
	           
	        	ae.printStackTrace();
	        }
	        catch (MessagingException me) {
	            me.printStackTrace();
	        }
		}
    }
	
	/**
	 * returns the title of the source for the given ID
	 * @param id the source id 
	 * @return a String of the source title
	 * @throws SQLException 
	 */
	public String getSource(String id) throws SQLException{
		Connection conn = null;
		
		Statement st = null;
	
			
			conn = DriverManager.getConnection(DB_ADDRS,DB_USER,DB_PASS);
			ResultSet rs=null;
			st= conn.createStatement();
			rs= st.executeQuery("SELECT Title FROM Source where Id="+id+";");
			 if (rs.isBeforeFirst()){ 
				 rs.next();
				 return Integer.toString(rs.getInt("Id"));
			 }else 
				 return null;
		
		
	}
	
	/**
	 * gets all the information passed and insert them to the database tables. All values given are deleted after the execution of the method
	 * It is using the 
	 * insertStats() to insert the stats of the machines and
	 * inserts() to insert the data for the messages
	 * @throws SQLException 
	 */
	public void runInsert() throws SQLException{
		Connection conn = null;
		Statement st = null;
		conn = DriverManager.getConnection(DB_ADDRS,DB_USER,DB_PASS);//"jdbc:mysql://127.0.0.1:3306/messenger","root","salonika");
	
		if(!inserted){
			inserted=true;
			//add the data on the primary tables
			String sourceId=SourceID;
			//find the application id, if already in the table. 
			//if not found insert it and then find it
			int appId=-1;
		
			ResultSet rs=null;
			st= conn.createStatement();
			rs= st.executeQuery("select Id from Application where Title='"+appName+"' and Dev_Name='"+devName+"' and SoftwareID='"+SoftwareID+"' and SourceID='"+sourceId+"';");
			 if (rs.isBeforeFirst()){
				 //application found. get the id
				 rs.next();
				 appId=  rs.getInt("Id");
			 }
			 else{
				 //application not found, insert it and then find it
				 st.executeUpdate("INSERT INTO Application (`SourceID`,`Title`,`Dev_Name`,`SoftwareID`) VALUES ("+sourceId+",'"+appName+"','"+devName+"','"+SoftwareID+"')");
				 rs= st.executeQuery("select Id from Application where Title='"+appName+"' and Dev_Name='"+devName+"' and SoftwareID='"+SoftwareID+"';");
				 rs.next();
				 appId=  rs.getInt("Id");	
			 }
		
			 //insert the data to the execution table
			
			try {
				PreparedStatement pstmt =null;
				
				String query="INSERT INTO Executions (`AppId`,`User`,`Start`)VALUES(?,?,?)";//+appId+",'"+userName+"','"+new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()).toString()+"');";
				pstmt =
		    		    conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);	
				pstmt.setLong(1,appId);
				pstmt.setString(2,userName);
			
				pstmt.setTimestamp(3,new java.sql.Timestamp(start.getTime()));
				 
				int affectedRows=pstmt.executeUpdate();
				
				 if (affectedRows == 0) {
					 System.out.println("inserting Execution data failed, no rows affected.");
					 throw new SQLException("inserting Execution data failed, no rows affected.");
				 }
				 //get the new id
				 try (ResultSet generatedKeys = pstmt.getGeneratedKeys()) {
					 if (generatedKeys.next()) {
						 execId=generatedKeys.getInt(1);
					 }
					 else {
						 System.out.println("inserting Execution data failed, no ID obtained.");
						 throw new SQLException("inserting Execution data failed, no ID obtained.");
					 }
				 }
				
			} catch (SQLException e) {
				
				String select="	select Id from Executions where AppId=appId and User =userName and Start=new java.sql.Timestamp(timestamp);";
				System.out.println("Exception on Execution insert/get ID\n"+select);
				
				rs=null;
				st= conn.createStatement();
				rs= st.executeQuery(select);
				rs.next();
				execId=  rs.getInt("Id");
				rs.close();
				st.close();
				e.printStackTrace();
			}
		}		
		
		insertMessage(conn,messageList,execId);
		messageList.clear();
		
		insertStats(MachineStats,conn,execId);
		MachineStats.clear();
	}
	
	/**
	 * insert the stats of the machine to the database
	 * @param data a List<MachineStats> where the machine name,ip and stats are stored
	 * @param conn the connection to the database
	 * @param execId the executionID to be used as a foreign key on the table
	 * @throws SQLException 
	 */
	public void insertStats(List<MachineStats> data,Connection conn,int execId) throws SQLException{
		Statement st = null;
		if (data.size()<1)
			return;
		
		for (MachineStats ms:data){
		PreparedStatement pstmt =null;
		 int machineId=0;
		 int statId=0;
		 String query=null;
		
				//try to find the machine id from the machines table
				conn = DriverManager.getConnection(DB_ADDRS,DB_USER,DB_PASS);//"jdbc:mysql://127.0.0.1:3306/messenger","root","salonika");
					st = conn.createStatement();					
				
				ResultSet rs=null;
				query="select Id from Machines where Comment='"+ms.getMachine().getComment()+"' and IP='"+ms.getMachine().getIp()+"'";
				rs= st.executeQuery(query);
				 if (rs.isBeforeFirst()){
					 //machine found
					 rs.next();
					 machineId=  rs.getInt("Id");
				 }else{
					 //machine not found
					 
					 PreparedStatement insert = conn.prepareStatement("INSERT INTO `Machines` (`Ip`,`Comment`) VALUES (?,?);");
					 insert.setString(1,ms.getMachine().getIp() );
					 insert.setString(2,ms.getMachine().getComment());
 
					 //execute the insert
					 insert.executeUpdate();
					 insert.close();
					 
					 //get the machine id
					ResultSet machineIdResult= st.executeQuery(query);
					machineIdResult.next();
					 machineId=  machineIdResult.getInt("Id");					
					 machineIdResult.close();
					 
				 }//else
				rs.close();
			
				//now insert the stats
				String[] statData=ms.getStats().getData(); 
				String statDate="";
				query="INSERT INTO `Machine_Stats` (`Cpu`,`Ram`,`Network`,`Hdd`) VALUES (?,?,?,?);";
				pstmt =
		    		    conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);	
				for(int i=0;i<statData.length;i++){
					pstmt.setString(i+1,statData[i]);
				}
				int affectedRows=pstmt.executeUpdate();
				
				 if (affectedRows == 0) {
					 System.out.println("inserting stats data failed, no rows affected.");
					 throw new SQLException("inserting stats data failed, no rows affected.");
				 }
				 //get the new id
				 try (ResultSet generatedKeys = pstmt.getGeneratedKeys()) {
					 if (generatedKeys.next()) {
						 statId=generatedKeys.getInt(1);
						 statDate=ms.getDate();
					 }
					 else {
						 System.out.println("inserting stats data failed, no ID obtained.");
						 throw new SQLException("inserting stats data failed, no ID obtained.");
					 }
				 }
				 pstmt.close();
				 
				 //insert to the Exec_Stats table
				 pstmt =
			    		    conn.prepareStatement("INSERT INTO `Exec_Stats` (`ExecId`,`MachineId`,`StatsId`,`Date_Time`) VALUES (?,?,?,?);");
				 pstmt.setInt(1,execId);
				 pstmt.setInt(2,machineId);
				 pstmt.setInt(3,statId);
				 pstmt.setString(4,statDate);//new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()).toString());
				pstmt.executeUpdate();
				 pstmt.close();

		
			}//for
	}
//	/**
//	 * insert the message passed to the database on table Success
//	 * returns true if the insert was successful
//	 * false if it failed and
//	 * throws exception if the needed parameters are missing
//	 * @param msg
//	 * @throws Exception 
//	 */
	/**
	 * Insert the messages passed to the Messages table
	 * throws exception if the needed parameters are missing
	 * @param conn the database connection
	 * @param data a List<msgBody> containing the messages
	 * @param execId the execution ID of the database
	 * @throws SQLException
	 */
	public void insertMessage(Connection conn,List<msgBody> data,int execId) throws SQLException{
		if (data.size()<1)
			return;
		PreparedStatement pstmt =null;
	
			pstmt =
	    		    conn.prepareStatement("INSERT INTO Messages (`ExecId`, `Message`,`Date_Time`,`MsgTypeID`,`Public`,`Comments` )VALUES(?,?,?,?,?,?);");
			pstmt.setInt(1,execId);
		for (int i=0;i<data.size();i++){
				pstmt.setString(2,data.get(i).message());
				pstmt.setString(3,data.get(i).date());	
				pstmt.setString(4,data.get(i).type());
				pstmt.setBoolean(5,data.get(i).getPublic());
				if(data.get(i).getComment()==null){
					pstmt.setString(6,"");
				}else{
					pstmt.setString(6,data.get(i).getComment());
				}
			pstmt.execute();			
			}
	
	}
	
	
	/**
	 * insert the data with the messages to their proper tables.
	 * @param conn The connection to the database
	 * @param table the table to be used
	 * @param data a List<msgBody> containing the messages
	 * @param execId the executionID to be used as a foreign key on the table
	 * @throws SQLException 
	 */
	public void inserts(Connection conn,String table,List<msgBody> data,int execId) throws SQLException{
		if (data.size()<1)
			return;
		PreparedStatement pstmt =null;
	
			pstmt =
	    		    conn.prepareStatement("INSERT INTO "+table+" (`ExecId`, `Message`,`Date_Time`)VALUES(?,?,?);");
			pstmt.setInt(1,execId);
		for (int i=0;i<data.size();i++){
				pstmt.setString(2,data.get(i).message());
				pstmt.setString(3,data.get(i).date());	
				pstmt.execute();			
			}
		
	}

}
